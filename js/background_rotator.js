
const isInsideElectron = function()
{
    return !!(window && window.process && window.process.versions && window.process.versions.electron)
}

const background_buttons = function()
{
    if( isInsideElectron() )
    {
        const remote = require( 'electron' ).remote

        $j('#btnMinimize')
        .click( function ()
        {
            var browserWindow = remote.getCurrentWindow()

            browserWindow.minimize()
        })

        $j('#btnMaximize')
        .click( function ()
        {
            var browserWindow = remote.getCurrentWindow()

            if( ! browserWindow.isMaximized() )
            {
                browserWindow.maximize()
            }
            else
            {
                browserWindow.unmaximize()
            }
        })

        $j('#btnClose')
        .click( function ()
        {
            var browserWindow = remote.getCurrentWindow()

            browserWindow.close()
        })
    }
}

const background_rotator = function()
{
    var numberOfImages = 0

	$container = $j('#container')
    $images = $container.find('img')
    $images.hide()
    numberOfImages = $images.length

    // $images.not(":first-child").hide()
    // $images.eq(0).addClass('visible')

    var currentImage = Math.floor( Math.random() * numberOfImages )

    $images.eq(currentImage).show()
    $images.eq(currentImage).addClass('visible')


    const showNextImage = function()
    {
        let nextImage = Math.floor( Math.random() * numberOfImages )
        // let nextImage = currentImage + 1
        // if( nextImage >= numberOfImages )
        // {
        //     nextImage = 0
        // }

        $container.find(".visible")
        .velocity( 'fadeOut',
        {
            duration: 500,
            complete: function()
            {
                $(this).removeClass('visible')
                $images.eq( nextImage ).addClass('visible')

                $images.eq( nextImage )
                .velocity( 'fadeIn',
                {
                    duration: 500,
                    complete: function()
                    {
                        currentImage = nextImage
                    },
                })
            },
        })
    }

    let seconds = 1000
    // setTimeout( showNextImage, 3*seconds )
    // setInterval( showNextImage, 3*seconds )
    setInterval( showNextImage, 120*seconds )

    background_buttons()
}
